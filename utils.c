#include "utils.h"

void debug(char* message)
{
    if(DEBUG)
        printf("[DEBUG]: %s\n",message);
}
Annuaire chargerAnnuaire()
{
    Annuaire annuaire;
    annuaire.taille = 0;
    annuaire.personnes = (Personne*)malloc(sizeof (Personne));
    FILE* fichier = NULL;
    fichier= fopen("annuaire.txt","r");
    if(!fichier)
    {
        printf("Nous avons pas trouve le fichier\n");
    }else{
        int ligne;

        while(!feof(fichier))
        {
            char * nom = (char*)malloc(sizeof (char) * 25 );
            char * prenom = (char*)malloc(sizeof (char) * 25);
            char * naissance = (char*)malloc(sizeof (char) * 10);
            char * nationalite = (char*)malloc(sizeof (char) * 25);
            Role role;
            ligne = fscanf(fichier,"%s",nom);
            ligne = fscanf(fichier,"%s",prenom);
            nom = strcat(nom," ");
            nom = strcat(nom,prenom);

            ligne = fscanf(fichier,"%s\n",naissance);
            ligne = fscanf(fichier,"%d",&role);
            ligne = fscanf(fichier,"%s",nationalite);

            printf("Lecture: %s %s %s %d ligne: %d\n",nom,naissance,nationalite,role,ligne);
            ajouterPersonne(&annuaire,nom,naissance,nationalite,role);
        }

    }
    fclose(fichier);
    printf("Annuaire charge avec succes\n");

    return annuaire;

}
Affiche  chargerAffichage(Annuaire* annuaire)
{
    /**
     * @brief affiche
     *     char * titre;
    int annee;
    Personne* realisateur;
    Personne* acteurs[3];
    int duree;
    Genre genre;
     */
    Affiche affiche;
    affiche.taille = 0;
    affiche.films = (Film*)malloc(sizeof (Film));
    FILE* fichier = NULL;
    fichier= fopen("affiche.txt","r");
    if(!fichier)
    {
        printf("Nous avons pas troue le fichier\n");
    }else{
        while(!feof(fichier))
        {
            char * titre = (char*)malloc(sizeof (char) * 25);
            int  annee;
            char * realisateur_nom = (char*)malloc(sizeof (char) * 25);
            char * realisateur_prenom = (char*)malloc(sizeof (char) * 25);
            char * acteur1_nom = (char*)malloc(sizeof (char) * 25);
            char * acteur1_prenom = (char*)malloc(sizeof (char) * 25);
            char * acteur2_nom = (char*)malloc(sizeof (char) * 25);
            char * acteur2_prenom = (char*)malloc(sizeof (char) * 25);
            char * acteur3_nom = (char*)malloc(sizeof (char) * 25);
            char * acteur3_prenom = (char*)malloc(sizeof (char) * 25);
            int duree;
            Genre genre;

            fscanf(fichier,"%s",titre);
            fscanf(fichier,"%d",&annee);
            fscanf(fichier,"%d",&duree);
            fscanf(fichier,"%s",realisateur_nom);
            fscanf(fichier,"%s",realisateur_prenom);

            realisateur_nom = strcat(realisateur_nom," ");
            realisateur_nom = strcat(realisateur_nom,realisateur_prenom);

            fscanf(fichier,"%s",acteur1_nom);
            fscanf(fichier,"%s",acteur1_prenom);
            acteur1_nom = strcat(acteur1_nom," ");
            acteur1_nom = strcat(acteur1_nom,acteur1_prenom);


            fscanf(fichier,"%s",acteur2_nom);
            fscanf(fichier,"%s",acteur2_prenom);
            acteur2_nom = strcat(acteur2_nom," ");
            acteur2_nom = strcat(acteur2_nom,acteur2_prenom);


            fscanf(fichier,"%s",acteur3_nom);
            fscanf(fichier,"%s",acteur3_prenom);
            acteur3_nom = strcat(acteur3_nom," ");
            acteur3_nom = strcat(acteur3_nom,acteur3_prenom);

            fscanf(fichier,"%d",&genre);


            Personne* p_realisateur = (Personne*)malloc(sizeof (Personne));
            if(!obtenirPersonne(annuaire,realisateur_nom,p_realisateur))
            {
                printf("Realisateur %s introuvable\n", acteur1_nom);
            }
            Personne** acteurs = (Personne**)malloc(sizeof (Personne*)* 3);


            Personne* p_acteur_1 = (Personne*)malloc(sizeof (Personne));
            if(obtenirPersonne(annuaire,acteur1_nom,p_acteur_1))
            {
                acteurs[0] = p_acteur_1;
            }else{
                printf("Acteur %s introuvable\n", acteur1_nom);
            }
            Personne* p_acteur_2 = (Personne*)malloc(sizeof (Personne));
            if(obtenirPersonne(annuaire,acteur2_nom,p_acteur_2))
            {
                printf("RESULTAT: %s\n",p_acteur_2->nom);
                acteurs[1] = p_acteur_2;
            }else{
                printf("Acteur %s introuvable\n", acteur2_nom);
            }
            Personne* p_acteur_3 = (Personne*)malloc(sizeof (Personne));
            if(obtenirPersonne(annuaire,acteur3_nom,p_acteur_3))
            {
                acteurs[2] = p_acteur_3;
            }else{
                printf("Acteur %s introuvable\n", acteur3_nom);
            }

            ajouterFilm(&affiche,titre,annee,p_realisateur,acteurs,duree,genre);

        }

    }
    fclose(fichier);
    printf("Affichage charge avec succes\n");

    return affiche;
}

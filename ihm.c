#include "ihm.h"

int demanderUtilisateur(int bornSup,int bornInf, char* message)
{
    int reponse;
    do {
        printf(message, bornSup,bornInf);
        scanf("%d",&reponse);
    } while (bornSup>=reponse && bornInf<=reponse);
    printf("Vous avez selectionne la solution %d\n",reponse);
    return reponse;
}

#ifndef PERSONNE_H
#define PERSONNE_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef enum{
    REALISATEUR,
    ACTEUR,
    ACTEUR_ET_REALISATEUR,
}Role;


typedef struct{
    char* nom;
    char* naissance;
    char* nationalite;
    Role role;
}Personne;

typedef struct Annuaire{
    unsigned int taille;
    Personne* personnes;
}Annuaire;

// SELECT AND DISPLAY
void afficherRealisateurs(Annuaire*);
void afficherActeurs(Annuaire*);

//INSERT
int ajouterPersonne(Annuaire*, char*,char*,char*,Role);

//EXISTS
int personneExiste(Annuaire*,Personne,int*);

//UPDATE
int miseAjour(Annuaire*,int,Role);

int obtenirPersonne(Annuaire* query,char* nom, Personne *);

char * obtenirRole(Role);


#endif // PERSONNE_H

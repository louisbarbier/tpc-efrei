#include "film.h"

/**
 * @brief obtenirGenre
 * @param genre
 * @return
 */
char * obtenirGenre(Genre genre)
{
    switch (genre)
    {
    case ACTION:
    return "Action";
        break;
    case HORREUR:
    return "Horreur";
    break;
    case COMEDIE:
    return "Comedie";
    break;
    case DOCUMENTAIRE:
    return "Documentaire";
    break;
    case POLICIER:
    return "Policier";
    break;
    case DRAME:
    return "Drame";
    break;
    case ANIMATION:
    return "Animation";
    break;
    case SF:
    return "Science fiction";
    break;
    default:
    return "Inconnu";
        break;
    }
}
/**
 * @brief filmExiste
 * @param query
 * @param film
 * @param positionResult
 * @return
 */
int filmExiste(Affiche * query,Film film,int* positionResult)
{
    /*
     * Un film existe si le titre et le réalisateur est le même (un film peut être réalisé plusieurs fois)
     * Signaux de retour:
     * 0: N'existe pas
     * 1: Existe
     *
     * Envoie un parametre int* pour stocker la position de l'occurence si elle existe
     * */

    unsigned int i;
    for(i=0;i<query->taille;++i)
    {
        if(film.titre == query->films[i].titre && film.realisateur == query->films[i].realisateur)
        {
            *positionResult = i;
            return 1;
        }
    }
    return 0;
}
/**
 * @brief ajouterFilm Ajoute un film
 * @param query La base de donnée
 * @param annee Année
 * @param realisateur Réalisateur
 * @param acteurs Acteurs principaux
 * @param duree Durée du film
 * @param genre Genre du film
 * @return
 */
int ajouterFilm(Affiche * query,char* titre,int annee,Personne* realisateur,Personne* acteurs[3],int duree,Genre genre)
{
    /**
      Signaux de retour:
      -1: Artiste deja present dans la base
      1: Artiste ajouté ou modifié avec succès
      */
    Film film;
    film.titre = titre;
    film.annee = annee;
    film.realisateur = realisateur;
    int i;
    for (i=0;i<3;++i)
    {
        film.acteurs[i] = acteurs[i];
    }
    film.duree = duree;
    film.genre = genre;

    //Controler si la personne est deja dans la base de donnee
    int occurence;
    int resultat;
    if((resultat = filmExiste(query,film,&occurence)))
    {
        printf("Le film est deja present dans la base de donnee\n");
        return 0;
    }
    query->taille++;
    query->films = (Film*) realloc(query->films, sizeof (query->films) + sizeof (film));
    query->films[query->taille - 1] = film;
    printf("Ajout de %s avec succes.\n",query->films[query->taille-1].titre);

    return 1;
}

/**
 * @brief afficherFilms Affiche tous les films
 * @param query Base de donnée
 */
void afficherFilms(Affiche * query)
{
    unsigned int i,j;
    printf("Nombre de film a afficher: %d\n",query->taille);
    for (i=0;i<query->taille; ++i)
    {
        printf("Titre:%s\n", query->films[i].titre);
        printf("Annee:%d\n", query->films[i].annee);
        printf("Realisateur: %s\n",query->films[i].realisateur->nom);
        printf("Acteurs:\n");
        for (j=0;j<3;++j)
        {
            printf("%s",
            query->films[i].acteurs[j]->nom);
            if (j!=2)
            {
                printf(", ");
            }else{
                printf("\n");
            }
        }
        printf("Duree: %d\n", query->films[i].duree);

    }
}
/**
 * @brief obtenirFilm Obtenir un film
 * @param query Base de donnée
 * @param titre Titre de film
 * @param resultat Résultat
 * @return 1 ou 0
 */
int obtenirFilm(Affiche* query,char* titre,Film* resultat)
{
    unsigned int i;
    for (i=0;i<query->taille;++i)
    {
        if(titre == query->films[i].titre)
        {
            resultat = &query->films[i];
            return 1;
        }
    }
    return 0;

}

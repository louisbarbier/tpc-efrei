TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        film.c \
        ihm.c \
        main.c \
        personne.c \
        utils.c

HEADERS += \
    film.h \
    ihm.h \
    personne.h \
    utils.h

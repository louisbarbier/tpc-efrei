#ifndef FILM_H
#define FILM_H
#include "personne.h"

typedef enum{
    ACTION,
    HORREUR,
    COMEDIE,
    DOCUMENTAIRE,
    POLICIER,
    DRAME,
    ANIMATION,
    SF,
}Genre;


typedef struct
{
    char * titre;
    int annee;
    Personne* realisateur;
    Personne* acteurs[3];
    int duree;
    Genre genre;
}Film;


typedef struct Affiche
{
    unsigned int taille;
    Film* films;

}Affiche;


char * obtenirGenre(Genre);
void afficherFilms(Affiche *);
int ajouterFilm(Affiche * ,char*,int ,Personne*,Personne* acteurs[3],int ,Genre );

int obtenirFilm(Affiche* query,char* titre, Film *);

#endif // FILM_H

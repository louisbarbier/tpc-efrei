#ifndef UTILS_H
#define UTILS_H
#define DEBUG 1
#include <string.h>
#include "film.h"
#ifdef __unix__
#define clearscreen "clean"

#elif defined(_WIN32) || defined(WIN32)
#define clearscreen "cls"

#endif

void debug(char*);
Annuaire  chargerAnnuaire();
Affiche chargerAffichage(Annuaire* annuaire);
#endif // UTILS_H

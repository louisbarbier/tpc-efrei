#include "personne.h"

void afficherPersonne(Personne* personne)
{
    printf("Affichage de %s\n",personne->nom);
    printf("Naissance: %s\n",personne->naissance);
    printf("Nationalite: %s\n",personne->nationalite);
    printf("Role: %s\n",obtenirRole(personne->role));
}

/**
 * @brief afficherRealisateurs Permet d'afficher les réalisateurs
 * @param query Base de donnée
 */
void afficherRealisateurs(Annuaire * query)
{
    unsigned int i;
    for (i=0;i<query->taille;++i)
    {
        if(query->personnes[i].role == REALISATEUR || query->personnes[i].role == ACTEUR_ET_REALISATEUR)
        {
            printf("Nom:%s\n",(query->personnes[i]).nom);
            printf("Date de naissance: %s\n",(query->personnes[i]).naissance);
            printf("Nationalite: %s\n",(query->personnes[i]).nationalite);
        }
    }

}

/**
 * @brief afficherActeurs Permet d'afficher tous les acteur
 * @param query Base de donnée
 */
void afficherActeurs(Annuaire * query)
{
    unsigned int i;
    for (i=0;i<query->taille;++i)
    {
        if(query->personnes[i].role == ACTEUR || query->personnes[i].role == ACTEUR_ET_REALISATEUR)
        {
            printf("Nom:%s\n",(query->personnes[i]).nom);
            printf("Date de naissance: %s\n",(query->personnes[i]).naissance);
            printf("Nationalite: %s\n", (query->personnes[i]).nationalite);

        }
    }

}
/**
 * @brief obtenirRole
 * @param role
 * @return
 */
char * obtenirRole(Role role)
{
    switch (role)
    {
    case REALISATEUR:
        return "Realisateur";
        break;
        case ACTEUR:
        return "Acteur";
        break;
        case ACTEUR_ET_REALISATEUR:
        return "Acteur et réalisateur";
        break;
    default:
    return "Inconnu";
        break;
    }
}
/**
 * @brief personneExiste Savoir si la personne existe das la base de donnée
 * @param query Base de donnée
 * @param personne La personne
 * @param positionResult
 * @return
 */
int personneExiste(Annuaire * query,Personne personne,int* positionResult)
{
    /*
     *Permet de vérifier si un personne qui existe dans la base de donnée.
     * Signaux de retour:
     * 0: N'existe pas
     * 2: Existe mais n'a pas le même role
     * 1: Existe
     *
     * Pour identifier une personne on compare le nom ainsi que la date de naissance
     *
     * Envoie un parametre int* pour stocker la position de l'occurence si elle existe
     * */

    unsigned int i;
    for(i=0;i<query->taille;++i)
    {

        if((!strcmp(personne.nom,query->personnes[i].nom))&&(!strcmp(personne.naissance,query->personnes[i].naissance)))
        {
            *positionResult = i;
            if(personne.role == query->personnes[i].role)return 1;
            return 2;
        }
    }
    return 0;

}
int miseAjour(Annuaire * query,int index,Role role)
{
    /*
     *Index; index dans le tableau
     * Query: Ensemble de données (base de donnée)
     * -1: echec
     * 1: succés
     * */
    query->personnes[index].role = role;
    return 1;

}

/**
 * @brief ajouterPersonne Ajouter une personne
 * @param query Base de donnée
 * @param nom Nom et prenom de la personne
 * @param naissance Date de naissance
 * @param nationalite Nationalité
 * @param role Role de la personne (Acteur, Réalisateur, ou les deux)
 * @return
 */
int ajouterPersonne(Annuaire * query, char* nom,char* naissance,char* nationalite,Role role)
{
    /**
      Signaux de retour:
      0: Artiste deja present dans la base
      1: Artiste ajouté ou modifié avec succès
      */
    Personne personne;
    personne.nom = nom;
    personne.naissance = naissance;
    personne.nationalite = nationalite;
    personne.role = role;

    //Controler si la personne est deja dans la base de donnee
    int occurence;
    int resultat;
    if((resultat = personneExiste(query,personne,&occurence)))
    {
        if(resultat == 2)
        {
            printf("L'artiste existe deja mais nous allons lui ajouter le role %s.\n",obtenirRole(personne.role));
            //Ajout du role (l'occurence est stockée à l'adresse occurence)
            miseAjour(query,occurence,personne.role); //(vivement le c++ pour les references :) )
            return 1;
        }
        printf("L'artiste est deja present dans la base de donnee\n");
        return 0;

    }else{
        query->taille ++;
        query->personnes = (Personne*) realloc(query->personnes, query->taille * sizeof (Personne));
        query->personnes[query->taille - 1] = personne;
        printf("Ajout de %s avec succes (%d).\n",query->personnes[query->taille-1].nom,query->taille);
    }
    return 1;
}
int obtenirPersonne(Annuaire* query,char* nom,Personne* resultat)
{
    unsigned int i;
    for (i=0;i<query->taille;++i)
    {
        if(!strcmp(nom,query->personnes[i].nom))
        {
            resultat = &query->personnes[i];
            return 1;
        }
    }
    return 0;
}

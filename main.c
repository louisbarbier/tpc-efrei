#include "utils.h"
#include "ihm.h"

int main(void)
{

    Annuaire an = chargerAnnuaire();
    Annuaire* annuaire = (Annuaire*)malloc(sizeof (an));
    *annuaire = an;
    ajouterPersonne(annuaire,"Leonardo DiCaprio","25/01/1965","FR",ACTEUR);
    ajouterPersonne(annuaire,"Geaorge Clooney","13/03/1958","FR",ACTEUR);
    ajouterPersonne(annuaire,"Stellan Skarsgard","13/06/1951","FR",ACTEUR);
    ajouterPersonne(annuaire,"Martin Klebba","23/06/1969","FR",ACTEUR);

    Affiche af = chargerAffichage(annuaire);
    Affiche * affiche = (Affiche*)malloc(sizeof (af));
    affiche = &af;
    //Boucle de logiciel
    //system(clearscreen);
    int action;
    do{

        action = demanderUtilisateur(1,3,"Que voulez vous faire? \n1: Afficher les realisateurs \n2: Afficher les acteurs ?\n3: Afficher les films\n4: Quitter\n");
        system(clearscreen);
        switch (action) {
        case 1:
            afficherRealisateurs(annuaire);
            break;
        case 2:
            afficherActeurs(annuaire);
            break;
        case 3:
            afficherFilms(affiche);
            break;
        case 4:
            printf("Fermeture du programme...\n");
            break;

        default:
            printf("Erreur\n");
            return 1;
        }
        fflush(stdin);
        printf("Appuyez sur n'importe quelle touche pour continuer\n");
        getchar();
        system(clearscreen);
    }while(action<4 && action>0);


    //Fermture du programme

    if( affiche != NULL)
    {
        free(affiche->films);
        free(affiche);
    }
    if(annuaire != NULL)
    {
        free(annuaire->personnes);
        free(annuaire);
    }



    return 0;
}
